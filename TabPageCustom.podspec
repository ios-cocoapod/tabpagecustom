#
# Be sure to run `pod lib lint TabPageCustom.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TabPageCustom'
  s.version          = '0.1.1'
  s.summary          = 'A short description of ios-tabpagecustom.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'git@gitlab.com:ios-cocoapod/tabpagecustom.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pimmii' => 'pimpun.dev@gmail.com' }
  s.source           = { :git => 'git@gitlab.com:ios-cocoapod/tabpagecustom.git',
                         :tag => '#{spec.version}' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform              = :ios, '5.0'
  s.ios.deployment_target = '10.0'

  s.source_files     = 'TabPageCustom/Classes/**/*'
  # s.resources        = 'TabPageCustom/Assets/**/*.{png, jpeg, jpg, storyboard, xib, xcassets}'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
