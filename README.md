# TabPageCustom

[![CI Status](https://img.shields.io/travis/Pimmii/TabPageCustom.svg?style=flat)](https://travis-ci.org/Pimmii/TabPageCustom)
[![Version](https://img.shields.io/cocoapods/v/TabPageCustom.svg?style=flat)](https://cocoapods.org/pods/TabPageCustom)
[![License](https://img.shields.io/cocoapods/l/TabPageCustom.svg?style=flat)](https://cocoapods.org/pods/TabPageCustom)
[![Platform](https://img.shields.io/cocoapods/p/TabPageCustom.svg?style=flat)](https://cocoapods.org/pods/TabPageCustom)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TabPageCustom is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TabPageCustom'
```

## Author

Pimmii, pimpun.dev@gmail.com

## License

TabPageCustom is available under the MIT license. See the LICENSE file for more info.
