//
//  ViewController.swift
//  TabPageCustom
//
//  Created by Pimmii on 10/20/2020.
//  Copyright (c) 2020 Pimmii. All rights reserved.
//

import UIKit
import TabPageCustom

class ViewController: TabPageViewController {
    override init() {
        super.init()
        setupChild()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupChild()
    }
    
    private func setupChild() {
//        view.backgroundColor = .black
        let vc1 = UIViewController()
        vc1.view.backgroundColor = UIColor.blue
        let vc2 = UIViewController()
        vc2.view.backgroundColor = UIColor.purple
        let vc3 = UIViewController()
        vc3.view.backgroundColor = UIColor.yellow
        let vc4 = UIViewController()
        vc4.view.backgroundColor = UIColor.systemPink
        let vc5 = UIViewController()
        vc5.view.backgroundColor = UIColor.orange
        let vc6 = UIViewController()
        vc6.view.backgroundColor = UIColor.red
        let vc7 = UIViewController()
        vc7.view.backgroundColor = UIColor.brown
        
        tabItems = [(vc1, "First"), (vc2, "Second"), (vc3, "Mon."), (vc4, "Tue."), (vc5, "Wed."), (vc6, "Thu."), (vc7, "Fri.")]
    }
}

